{
  "basics": {
    "name": "Jamie Spittal",
    "label": "Fullstack Developer",
    "picture": "https://firebasestorage.googleapis.com/v0/b/portfolio2017-23098.appspot.com/o/jamiesface.jpg?alt=media&token=c3fa6c71-a232-421a-8edd-222ba687b85d",
    "email": "jamie@jamiespittal.com",
    "phone": "778-689-5414",
    "website": "https://jamiespittal.com",
    "summary": "Jamie has been a Fullstack and Lead Developer for over 9 years. His interest in computers began at a young age while watching his father program their family’s Commodore 64 in the 90s and making goofy games himself on his TI-83.  Besides being an exemplary geek, Jamie is also an audiophile, having attended The Art Institute for Audio Production and worked on numerous film sets, doing everything from location audio to composing music for short films. It was between these gigs that Jamie used to learn new programming languages, which turned into his day job when he started a small web design and development company with a friend, working with clients across North America. Later on, looking to shift into a faster gear, Jamie joined Skyrocket as lead developer, where he worked on modernizing their development workflow, and delivering stunning web experiences for a broad range of clients.  Sometimes you can see Jamie biking around the city or enjoying a comedy show. But it’s surely difficult to get him away from his sweet three-monitor setup.",
    "location": {
      "address": "2770 Sophia St",
      "postalCode": "V5T0A4",
      "city": "Vancouver",
      "countryCode": "BC",
      "region": "Lower Mainland"
    },
    "profiles": [
      {
        "network": "Gitlab",
        "username": "Spittal",
        "url": "https://gitlab.com/Spittal"
      },
      {
        "network": "Github",
        "username": "Spittal",
        "url": "https://github.com/Spittal"
      },
      {
        "network": "LinkedIn",
        "username": "Jamie Spittal",
        "url": "https://www.linkedin.com/in/jamiespittal"
      }
    ]
  },
  "showcase": [
    {
      "title": "Omnifilm Productions",
      "image": "/static/omnifilm.jpg",
      "website": "https://omnifilm.com",
      "summary": "Built with Angular2. Clients can use Bentobox, an admin panel created by me to enter content."
    },
    {
      "title": "Skidmore Group",
      "image": "/static/skidmore.jpg",
      "website": "https://skidmoregroup.com/",
      "summary": "Built with CraftCMS. Leveraging a modern workflow that includes Webpack2, cssnext, and ES6."
    }
  ],
  "work": [
    {
      "company": "Skyrocket",
      "position": "Lead Developer",
      "website": "https://skyrocket.is",
      "startDate": "2015-10-10",
      "endDate": "Present",
      "summary": "Digital branding agency based in Vancouver BC",
      "highlights": [
        "Instituted a \"value-based\" approach to development. Values included: \"Code for someone else\", and \"Low Coupling, High Cohesion\"",
        "Modernized development workflow using Docker, Kubernetes, Continuous Integration and Continuous Deployment",
        "Acquired invaluable skills in: People management, large software maintenance and software vetting",
        "Helped launch over a dozen clients begin and improve their online presence.",
        "Used tools such as Vue2, Laravel5, and Angular2 to develop beautiful web apps"
      ],
      "highlightTitles": [
        "Values",
        "Workflow",
        "Management",
        "Scale",
        "Technology"
      ]
    },
    {
      "company": "Bentobox",
      "position": "Lead Developer",
      "website": "https://www.behance.net/gallery/49634743/Bentobox-Brand-Identity",
      "startDate": "2016-10-10",
      "endDate": "Present",
      "endDate": "Present",
      "summary": "An in-progress open source drop-in admin panel for Laravel5 Projects",
      "highlights": [
        "Created a highly functional and unbelievably easy to set-up admin panel",
        "Refined high-level PHP skills",
        "Learned valuable skills in open source project management"
      ]
    },
    {
      "company": "Closefox",
      "position": "Lead Developer",
      "website": "https://closefox.com",
      "startDate": "2014-07-01",
      "endDate": "2015-10-01",
      "summary": "Closefox is a sales and marketing collateral data collection tool that allows users see exactly how their prospects are engaging with their content.",
      "highlights": [
        "Single handedly developed the front-end facing panel in AngularJS.",
        "Helped with architecture and development of a Laravel based web-api.",
        "Consulted with design, branding and marketing.",
        "Worked in a tight knit six person team.",
        "Learned valuable \"devops\" such as deployments with Docker"
      ]
    }
  ],
  "volunteer": [],
  "education": [],
  "awards": [],
  "publications": [],
  "skills": [
    {
      "name": "Core Web Development",
      "level": "Master",
      "keywords": [
        "HTML",
        "CSS",
        "Javascript"
      ]
    },
    {
      "name": "Frontend Frameworks",
      "level": "Master",
      "keywords": [
        "Angular",
        "Angular2",
        "VueJS"
      ]
    },
    {
      "name": "Backend Frameworks",
      "level": "Master",
      "keywords": [
        "Laravel"
      ]
    },
    {
      "name": "Devops",
      "level": "Master",
      "keywords": [
        "Docker",
        "Kubernetes",
        "AWS",
        "Google Cloud"
      ]
    }
  ],
  "languages": [
    {
      "language": "English",
      "fluency": "Native speaker"
    },
    {
      "language": "Mandarin",
      "fluency": "Basic Knowledge"
    }
  ],
  "interests": [
    {
      "name": "Audio/Music",
      "keywords": [
        "Coding Generative Music with MAX",
        "Field Recording",
        "Film Post Production Sound"
      ]
    },
    {
      "name": "Recreation",
      "keywords": [
        "Disc Golf",
        "Ultimate Frisbee",
        "Squash"
      ]
    },
    {
      "name": "Science / Mathmatics",
      "keywords": [
        "Philosophy",
        "Astrophysics",
        "Cryptography"
      ]
    }
  ],
  "references": [
    {
      "name": "Matt Hall",
      "reference": "For many years Jamie and I have worked together to create and operate Random Shapes, Growler Fill, Ferries and various other projects. He's a skilled and detailed developer that operates with high integrity."
    }
  ]
}
